<?php

defined('MOODLE_INTERNAL') || die;


$settings->add(new admin_setting_configtext(
            'pcavailability/dbserver',
            get_string('labeldbserver', 'block_pcavailability'),
            get_string('descdbserver', 'block_pcavailability'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'pcavailability/dbport',
            get_string('labeldbport', 'block_pcavailability'),
            get_string('descdbport', 'block_pcavailability'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'pcavailability/dbname',
            get_string('labeldbname', 'block_pcavailability'),
            get_string('descdbname', 'block_pcavailability'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'pcavailability/dbuser',
            get_string('labeldbuser', 'block_pcavailability'),
            get_string('descdbuser', 'block_pcavailability'),
            ''
        ));
$settings->add(new admin_setting_configpasswordunmask(
            'pcavailability/dbpassword',
            get_string('labeldbpassword', 'block_pcavailability'),
            get_string('descdbpassword', 'block_pcavailability'),
            ''
        ));