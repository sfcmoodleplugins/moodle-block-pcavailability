<?php
class block_pcavailability extends block_base {
    public function init() {
        $this->title = get_string('pcavailability', 'block_pcavailability');
    }
     public function applicable_formats() {
        return array('all' => true);
    }
    function has_config() {
        return true;
    }
    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.
    public function get_content() {
    global $USER;
    if ($this->content !== null) {
      return $this->content;
    }

    $dbserver = get_config('pcavailability', 'dbserver');
    $dbport = get_config('pcavailability', 'dbport');
    $dbname = get_config('pcavailability', 'dbname');
    $dbuser = get_config('pcavailability', 'dbuser');
    $dbpassword = get_config('pcavailability', 'dbpassword');

    //$db_host = 'SVR-STATIONLOG';
    //$db_user = 'Availability';   //recommend using a lower privileged user
    //$db_pwd = 'bg42XLm09I';
    //$database = 'sessionmonitor';

    $serverName = "$dbserver, $dbport"; //serverName\instanceName, portNumber (1433 by default)
    $connectionInfo = array( "Database"=>"$dbname", "UID"=>"$dbuser", "PWD"=>"$dbpassword");

    //$link = mssql_connect($db_host, $db_user, $db_pwd);
    $conn = sqlsrv_connect( $serverName, $connectionInfo);

    if (!$conn)
        echo "Unable to connect";
    //if (!mssql_select_db('sessionmonitor', $conn))
    //    echo "Unable to select database";

    //$result = mssql_query('SELECT * FROM BPE_FUNDING');
    $sql = 'SELECT *  FROM Free_All';
    //$result = mssql_query($dbsql);
    $stmt = sqlsrv_query( $conn, $sql);
    $text = "";

    //$table = array();
    //while($r = mssql_fetch_array($result, MYSQL_ASSOC)) {
    while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

        //foreach($r as $k=>$v) {
            $total = $r['Total'];
            $busy = $r['Busy'];
            $free = $total - $busy;
            $percent = $free * 100 / $total;
            if ($percent <= 0) {
                $freespan = '<strong style="color: rgb(146,0,0)">'. max($free,0) .'</strong>';
            } elseif ($percent >= 80) {
                $freespan = '<strong style="color: rgb(0,146,0)">'. max($free,0) .'</strong>';
            } else {
                $freespan = max($free,0);
            }

            //$G=round(255*$percent/100);
            //$R=255-$G; 
            //$pcol= '<strong style="color: rgb('.$R.','.$G.',0)">'.$nxm['s']['kom'].'</strong>';
            //$text = $text . $r['Name'] . ' : ' .'<style="color: rgb('.$colour.')">'. $free .'</style>'. '</br>'; 
            //$text = $text . $r['Name'] . ' : ' . $free . '</br>'; 
            $text = $text . $r['Name'] . ' : ' . $freespan  . '</br>';

            
        //}
    }
    //mssql_free_result($result);
    sqlsrv_free_stmt( $stmt);

    $linktext = "";
    if (! empty($this->config->linktext)) {
      $linktext = $this->config->linktext;
    }
    $linkurl = "";
    if (! empty($this->config->linkurl)) {
      $linkurl = $this->config->linkurl;
    }

    $this->content         =  new stdClass;
    $this->content->text   = $text;
    $this->content->text  .= '</br><p align="center"><a href='.$linkurl.'>'.$linktext.'</span></a></p>';
 
    //$this->content->footer = 'Footer here...';
 
    return $this->content;
  }
}   // Here's the closing bracket for the class definition