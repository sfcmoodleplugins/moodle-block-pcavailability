<?php
$string['pluginname'] = 'PC Availability Widget block';
$string['pcavailability'] = 'Available Open Access PCs';
$string['linktext'] = 'Link Text for PC Availability Widget';
$string['linkurl'] = 'URL for PC Availability Widget';
$string['papercut:addinstance'] = 'Add a new PC Availability Widget block';
$string['papercut:myaddinstance'] = 'Add a new PC Availability Widget block to the My Moodle page';

$string['labeldbserver'] = 'Database Server:';
$string['descdbserver'] = 'Must be the format of DNSName\InstanceName';
$string['labeldbport'] = 'Database Port:';
$string['descdbport'] = 'SQL server port';
$string['labeldbname'] = 'Database Name:';
$string['descdbname'] = 'SQL Database Name';
$string['labeldbuser'] = 'Database User:';
$string['descdbuser'] = 'Username to connect to database';
$string['labeldbpassword'] = 'Database User Password:';
$string['descdbpassword'] = 'Password for Database User';