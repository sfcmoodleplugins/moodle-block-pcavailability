<?php
 
class block_pcavailability_edit_form extends block_edit_form {
 
    protected function specific_definition($mform) {
 
        // Section header title according to language file.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));
 
        // A sample string variable with a default value.
        $mform->addElement('text', 'config_linktext', get_string('linktext', 'block_pcavailability'));
        $mform->setType('config_linktext', PARAM_MULTILANG);
 
 		$mform->addElement('text', 'config_linkurl', get_string('linkurl', 'block_pcavailability'));
        $mform->setType('config_linkurl', PARAM_MULTILANG);
    }
}