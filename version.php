<?php
$plugin->version = 2018060113;  // YYYYMMDDHH (year, month, day, 24-hr time)
$plugin->requires = 2010112400; // YYYYMMDDHH (This is the release version for Moodle 2.0)
$plugin->component = 'block_pcavailability'; // Declare the type and name of this plugin.